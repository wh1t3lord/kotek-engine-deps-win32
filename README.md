# kotek-engine-deps-win32

Win32 deps folder for kotek-engine. All compiled dependencies are built with msvc compiler.

# Dependencies (already "offline" installed)

Offline installed libraries are libraries that install with command "cmake --install . --config YOUR_CMAKE_BUILD_TYPE".
It means we support only those plaforms that we develop on. 

Manually installed means that dependency was manually downloaded and was built and was installed with CMake. So if you want to have your own versions of dependencies that are built with different macros or compilers you need to make it on your own, because these dependencies are built and installed only for Windows development.

So if you see no in the field "manually installed" it means it's a header only dependency.

You need to remember that we don't change any source code of dependency that we use. It's really important, because some projects require the statement to not change the source code of project that you use.

"Must separate install dirs" means you have to do cmake --install . --config YourConfigurationName (see the table of configuration naming in kotek). So if it is "YES" means you must put every installation in separated folder that root is the name of YourConfigurationName.

For example, if "Must separate install dirs" equals to "YES" (see the table) it means the following:

( In some of your dependency in build folder ...)

cmake --install . --config Debug

C:/YourInstallPath/Debug/include/
C:/YourInstallPath/Debug/lib/

etc...

So for every other standard configuration names you will have the following in final dependency root folder (e.g. this repository):

literally-this-repository-folder-name/YourFolderOfDependency/Debug/ <= all contents of cmake install command
literally-this-repository-folder-name/YourFolderOfDependency/Release/ <= all contents of cmake install command
literally-this-repository-folder-name/YourFolderOfDependency/MinSizeRel/ <= all contents of cmake install command
literally-this-repository-folder-name/YourFolderOfDependency/RelWithDebInfo/ <= all contents of cmake install command

So to compare with normal situation it would be like this:

literally-this-repository-folder-name/YourFolderOfDepndency/ <= all contents with targets for every configuration_name

Not every project that is based on cmake have compatibility with installation command for every targets. For some cases you have to separate every installation in different folders not in one.

| Dependency name | Version | Manually installed | Must separate install dirs
| ----------- | ----------- | ----------- | ----------- |
| assimp | 5.2 | Yes | YES |
| Boost | 1.79 | Yes | No |
| Bullet | 3.24 | Yes | No |
| CMake | 3.23.1 | By User on your local machine | No |
| DirectXMath | 3.17 | No | No |
| Eigen | 3.4.0 | No | No |
| FMT | 8.1.2 | Yes | No |
| glfw3 | 3.4.0 | Yes | No |
| glm | 0.9.9.8 | No | No |
| imgui | 1.88 WIP (docking version) | No | No |
| KTX-Software | 4.0.0 | Yes | YES |
| mimalloc | 2.0 | Yes | YES |
| Tracy | 0.8.1 | No | No |
| shaderc | 1.5.5 (cloned from 05.06.2022) | Yes | YES |
| SPIRV-Reflect | Not stated (cloned from 05.06.2022) | No | No |
| oneTBB | 2021.7.0 | Yes | No |
| Visual Leak Detector | 2.5.1 | No (Windows only) | No |
| wxWidgets | 3.1.6  | Yes | No |
| Vulkan Memory Allocator (VMA) | 3.0.1 | No | No |
| glad3 | OpenGL 3.3 | No | No |
| glad4.6 | OpenGL 4.6 | No | No |

# License status 

Необходимо понимать что лицензирование текущего проекта при обновлении его же зависимостей может повлечь к состоянию когда текущая лицензия этого проекта будет несовместимой с новой лицензией какой-либо зависимости. Поэтому здесь приводится список всех лицензий всех зависимостей, и текущая лицензия которая используется

kotek-engine's (not this repository) license is Apache 2.0

ATTENTION: We appreciate your help in case when you show us where we're wrong and have incompatibale license with what we have

| Dependency's name | License | Compatibility with project's license | Explanation | 
| ----------- | ----------- | ----------- | ----------- |
| Boost | Boost Software license - Version 1.0 | Yes |  |
| Bullet | Zlib license | Yes |  |
| DirectXMath | MIT license | Yes |  |
| Eigen | MPL 2.0 license | Yes |  |
| FMT | MIT license | Yes | Yes |
| glfw3 | Zlib license | Yes |  |
| glm | MIT license | Yes |  |
| imgui | MIT license | Yes |  |
| KTX-Software | Apache-2.0 license | The same as project's license |  |
| mimalloc | MIT license | Yes | Yes |
| Tracy | 3-clause BSD license | Yes |  |
| shaderc | Apache-2.0 license | The same as project's license |  |
| SPIRV-Reflect | Apache-2.0 license | The same as project's license |  |
| oneTBB | Apache-2.0 license | The same as project's license |  |
| Visual Leak Detector | LGPL-2.1 license | Yes | We don't change the code and use only library for linking only and use dll file for 'working with library' |
| wxWidgets | wxWidgets license  | Yes | We don't alter the source code of WxWidgets library and use only for linking their pre-build static and dynamic libraries. As the result user gets only dynamic libraries after building the project. |
| Vulkan Memory Allocator | MIT license | Yes | |
| glad3 | The MIT License (MIT), Apache License Version 2.0, The EGL Specification | Yes | |
| glad4.6 | The MIT License (MIT), Apache License Version 2.0, The EGL Specification | Yes | |
