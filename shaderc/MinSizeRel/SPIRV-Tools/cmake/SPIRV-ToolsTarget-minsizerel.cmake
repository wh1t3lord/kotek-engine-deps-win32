#----------------------------------------------------------------
# Generated CMake target import file for configuration "MinSizeRel".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "SPIRV-Tools-static" for configuration "MinSizeRel"
set_property(TARGET SPIRV-Tools-static APPEND PROPERTY IMPORTED_CONFIGURATIONS MINSIZEREL)
set_target_properties(SPIRV-Tools-static PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_MINSIZEREL "CXX"
  IMPORTED_LOCATION_MINSIZEREL "${_IMPORT_PREFIX}/lib/SPIRV-Tools.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS SPIRV-Tools-static )
list(APPEND _IMPORT_CHECK_FILES_FOR_SPIRV-Tools-static "${_IMPORT_PREFIX}/lib/SPIRV-Tools.lib" )

# Import target "SPIRV-Tools-shared" for configuration "MinSizeRel"
set_property(TARGET SPIRV-Tools-shared APPEND PROPERTY IMPORTED_CONFIGURATIONS MINSIZEREL)
set_target_properties(SPIRV-Tools-shared PROPERTIES
  IMPORTED_IMPLIB_MINSIZEREL "${_IMPORT_PREFIX}/lib/SPIRV-Tools-shared.lib"
  IMPORTED_LOCATION_MINSIZEREL "${_IMPORT_PREFIX}/bin/SPIRV-Tools-shared.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS SPIRV-Tools-shared )
list(APPEND _IMPORT_CHECK_FILES_FOR_SPIRV-Tools-shared "${_IMPORT_PREFIX}/lib/SPIRV-Tools-shared.lib" "${_IMPORT_PREFIX}/bin/SPIRV-Tools-shared.dll" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
